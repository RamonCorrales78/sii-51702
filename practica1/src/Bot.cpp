#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "glut.h"
int main(){
	DatosMemCompartida *pdatos;
	int fd, x=1;
	char* org;
	float mitadRaqueta;

	//Abrir fichero
	fd=open("/tmp/DatosMemComp",O_RDWR);
	if(fd==-1){
		perror("Error al abrir el fichero de la memoria compartida\n");
		exit(1);
	}//Proyeccion en memoria
	org=(char*)mmap(NULL, sizeof(*(pdatos)), PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);
	if (org==MAP_FAILED)
	{
		perror("Error en la proyeccion del fichero origen");
		close(fd);
		exit(1);
	}	

	//Cerrar el descriptor de fichero.
	close(fd);

	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero creado en el paso 2.
	pdatos=(DatosMemCompartida*)org;

	//bucle infinito
	while(x==1){if(pdatos->accion==3)x=0;
	mitadRaqueta=((pdatos->raqueta1.y1+ pdatos->raqueta1.y2)/2);
	if(mitadRaqueta < (pdatos->esfera.centro.y))
		pdatos->accion=1; //Arriba
	else if(mitadRaqueta > (pdatos->esfera.centro.y))
		pdatos->accion=-1; //Abajo		
	else pdatos->accion=0; //Nada

	usleep(2500);
}
munmap(org,sizeof(*(pdatos)));
exit(1);
   

}	


