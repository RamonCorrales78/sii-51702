// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


#include <sys/file.h>
#include <sys/mman.h>
#include <fstream>
#include <fcntl.h>


#define LONG 50
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
	flag=0;
}

CMundoCliente::~CMundoCliente()
{	//mensaje fin a logger
	printf("Destructor mundocliente");
	char cadenaFin[16];
	sprintf(cadenaFin, "Fin del juego \n"); //nos cercioramos de que sea cadena
	

	//avisa al bot de que debe cerrarse
	pdatos->accion=3;
	munmap(org, sizeof(datos));
	
	unlink("/tmp/DatosMemComp");

	//Cerrar tubería coordenadas
	close(fd_coordenadas);
	unlink("/tmp/tuberiaCoordenadas");

	//Cerrar tuberia teclas
	write(fd_teclas, cadenaFin, strlen(cadenaFin)+1);
	close(fd_teclas);
	unlink("/tmp/tuberiaTeclas");


}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		char cadena2[LONG];
		sprintf(cadena2, "Jugador 2 anota, el total es de %d puntos\n",
puntos2);
		//write(fd, cadena2, strlen(cadena2)+1);

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		char cadena1[LONG];
		sprintf(cadena1, "Jugador 1 anota, el total es de %d puntos\n",
puntos1);
		//write(fd, cadena1, strlen(cadena1)+1);
	}

	pdatos->esfera=esfera;
	pdatos->raqueta1= jugador1;

	//LLamada al OnKeyBoardDown
	switch(pdatos->accion){
		case 1:
			OnKeyboardDown('w', 0, 0);
			break;	
		case -1:
			OnKeyboardDown('s',0,0);
			break;
		case 0:
			jugador1.velocidad.y=0;
			break;
		default:
			break;
		}
		//Fin del juego
	//if(puntos1==3 || puntos2==3) exit(1);
	//Lectura datos tuberia
	int read_error=read(fd_coordenadas, cad, sizeof(cad));
	if(read_error==-1){
		perror("Error al leer la tubería\n");
		exit(1);

	}
	if(cad[0]=='F') exit(1);
	
	//recibimos los datos y actualizamos MundoCliente
	sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);


}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
	sprintf(cad_teclas, "%c", key);	
	write(fd_teclas, cad_teclas, strlen(cad_teclas)+1);
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	/*//abrir tubería de coordenadas
	fd_coordenadas=open("/tmp/tuberiaCoordenadas", O_RDONLY);
	if(fd_coordenadas==-1){
		perror("Error al abrir la tubería de las coordenadas \n");
		exit(1);
	}*/
	//Crear un fichero del tamaño
	fd_datos=open("/tmp/DatosMemComp", O_RDWR|O_CREAT|O_TRUNC, 0777);

	if(fd_datos==-1){
			printf("Error al abrir fichero para la memoria compartida\n");
			close(fd_datos);
			exit(-1);
	}
	//Definir el tamaño del fichero
	write(fd_datos, &datos, sizeof(datos));

	//Se proyecta todo el fichero origen
	org=(char*)mmap(NULL, sizeof(datos), PROT_WRITE|PROT_READ, MAP_SHARED, fd_datos, 0); //Devuelve la dirección de proyeccion utilizada

	if(org==MAP_FAILED){
		perror("No puede abrir el fichero de proyeccion\n");
		exit(-1);
	}
	//Cerrar el descriptor de fichero	
	close(fd_datos);
	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero creada en el paso 2.
	pdatos=(DatosMemCompartida*)org;
	pdatos->accion=0;

	//creacion fifo coordenadas
	
	unlink("/tmp/tuberiaCoordenadas");

	if(mkfifo("/tmp/tuberiaCoordenadas", 0777)==-1){
		perror("Error al crear la tubería de coord\n");
		exit(1);
	}



	//creacion fifo teclas
	unlink("/tmp/tuberiaTeclas");//eliminarlo si ya existe
	int error_teclas=mkfifo("/tmp/tuberiaTeclas", 0777);
	if(error_teclas==-1){
		perror("error al crear la tubería de las teclas\n");
		exit(1);
	}

	//apertura fifo coord
	fd_coordenadas=open("/tmp/tuberiaCoordenadas", O_RDONLY);

	if(fd_coordenadas==-1){
		perror("Error al abrir la tubería coord\n");
		exit(1);
	}
	else 
		printf("apertura fifo coord realizada\n");

	//apertura fifo teclas
	fd_teclas=open("/tmp/tuberiaTeclas", O_WRONLY);
	if(fd_teclas==-1){
		perror("Error al abrir la tubería teclas\n");
		exit(1);
	}

	else
		printf("apertura fifo teclas realizada");



}
