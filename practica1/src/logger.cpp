#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#define MAX 60

int main(void){
	
	char cadena[MAX];
	int fd;
	int create_error, read_error;
	
	create_error=mkfifo("/tmp/tuberia", 0777);
	if(create_error==-1){
		perror("Error al crear la tubería");
		exit(1);
	}
	
	fd=open("/tmp/tuberia", O_RDONLY);
	if(fd==-1){
		perror("Error al abrir la tuberia\n");
		exit(1);
	}
	printf("Inicio del Juego \n\n");
	

	for(;;){
	read_error=read(fd, cadena, sizeof(cadena));
	
	if(read_error==-1){
		perror("Error al leer la tubería \n");
		exit(1);
	}
	
	else if(cadena[0]=='F'){ //Pasamos en el destructor de Mundo el mensaje "Fin del juego"
		printf("%s", cadena);
		break;
	}

	else{
		printf("%s", cadena);

	}
}
		
	close(fd);
	unlink("/tmp/tuberia");
}
	

